# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 16:32:11 2020

@author: ai
"""
from aiohttp import web
import os
import json
import aiohttp_jinja2
import jinja2
import cv2
import io
import base64
import numpy as np

import API_inference


async def inference(request):
    data = await request.post()
    
    message = b''
    
    if not data['image']:
        message += str("Please select an image!").encode('UTF-8')
        return web.Response(body=message, content_type="text/html")
        
    
    image_buffer = data['image'].file.read()
    resultJson = API_inference.inference(image_buffer)    

    #is_success, buffer = cv2.imencode(".jpg", resultImg)
    #img_buffer = io.BytesIO(buffer)  
    
    message += str(resultJson).encode('UTF-8')
    message += '<br>'.encode('UTF-8')
    #message += '<img src="data:image/jpeg;base64,'.encode('UTF-8')
    #message += base64.b64encode(img_buffer.getvalue())
    #message += '"/><br>'.encode('UTF-8')      
    
    content = message
    return web.Response(body=content, content_type="text/html")
    


def load_model(args):

    API_inference.load_model(args)

    