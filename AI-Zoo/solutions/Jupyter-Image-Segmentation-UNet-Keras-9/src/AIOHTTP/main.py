# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 16:29:27 2020

@author: ai
"""

from aiohttp import web
from . import routes
import argparse
from .API import load_model


parser = argparse.ArgumentParser(description='Aiohttp server')
parser.add_argument('--port', type=str, default=8080,
                    help='port')
parser.add_argument('--model_file', type=str, default=None,
                    help='model path')
parser.add_argument('--threshold', type=float, required=False, default=None,
                    help='threshold (option)')
parser.add_argument('--config_file', type=str, required=False, default=None,
                    help='config_file (option)')
parser.add_argument('--device', type=str, required=False, default='gpu',
                    help='gpu or cpu')
parser.add_argument('--net', type=str, required=False, default=None,
                    help='network')
parser.add_argument('--label', type=str, required=False, default=None,
                    help='label')
parser.add_argument('--image_size', type=str, default='512',
                    help='UNet image size')
parser.add_argument('--image_channel', type=str, default='3',
                    help='UNet image channel')
args = parser.parse_args()
load_model(args)


app = web.Application()

routes.setup_routes(app)
routes.setup_static_routes(app)
routes.setup_template_routes(app)

web.run_app(app, host='127.0.0.1', port=args.port) 