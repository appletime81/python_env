from model import *
from data import *

import base64
import cv2

import io
import json

import numpy as np

import os
from PIL import Image

import skimage
import skimage.transform as trans
from skimage import img_as_ubyte

global model
global input_image_size
global input_image_channel


def load_model(args):
    
    global model
    global input_image_size
    global input_image_channel
    
    import tensorflow as tf
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
    
    input_image_size = int(args.image_size)
    input_image_channel = int(args.image_channel)
    
    model = unet(input_size=(input_image_size,input_image_size,input_image_channel))
    model.load_weights(args.model_file)


def test_one_Generator(image, target_size = (512,512),flag_multi_class = False,as_gray = True):

    img = trans.resize(image, target_size)
    if as_gray:
        img = np.reshape(img,img.shape+(1,)) if (not flag_multi_class) else img
    img = np.reshape(img,(1,)+img.shape)
    
    yield img


def apply_mask(image, mask, colors, alpha=0.5, flag_multi_class = False, num_class = 2):
    """Apply the given mask to the image.
    """
    if flag_multi_class == False:
        color = colors[0]
        for c in range(3):
            image[:, :, c] = np.where(mask > 0,
                                      image[:, :, c] *
                                      (1 - alpha) + alpha * color[c] * 255,
                                      image[:, :, c])
    return image


def random_colors(N, bright=True):

    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors

def imageToStr(image):
    
    buf = io.BytesIO()
    image.save(buf, format='JPEG')
    byte_im = buf.getvalue()
    
    image_byte = base64.b64encode(byte_im)
    image_str = image_byte.decode('ascii')

    return image_str

def StrToImage(image_str):
    
    image_str1= image_str.encode('ascii')
    image_byte = base64.b64decode(image_str1)
    image_json = open('mask_test.jpg', 'wb')
    image_json.write(image_byte)
    image_json.close()

def inference(image_buffer):
    
    global model
    global input_image_size
    global input_image_channel

    target_size = (input_image_size,input_image_size)
    flag_multi_class = False
    as_gray = True
    num_class = 2
    
    if input_image_channel ==3:
        as_gray = False
        target_size = (input_image_size,input_image_size, input_image_channel)
    
    
    image_file = cv2.imdecode(np.asarray(bytearray(image_buffer), dtype=np.uint8), 1)
    if input_image_channel ==3:
        image = cv2.cvtColor(image_file , cv2.COLOR_BGR2RGB)
    else:
        image = cv2.cvtColor(image_file , cv2.COLOR_BGR2GRAY)
    
    img = test_one_Generator(image, target_size=target_size, flag_multi_class=flag_multi_class, as_gray=as_gray)
    result = model.predict_generator(img, 1,verbose=1)
    
    item = result[0]
    img = labelVisualize(num_class,COLOR_DICT,item) if flag_multi_class else item[:,:,0]
    threshold = 0.5
    img = np.where(img > threshold, 255, 0)
    
    image_file = cv2.cvtColor(image_file , cv2.COLOR_BGR2RGB)      
    
    colors = random_colors(num_class - 1)
    img = apply_mask(image_file, img, colors, flag_multi_class=flag_multi_class, num_class=num_class)
    
   

    jsons = {}
    defectList = []	

    img_pilimg = Image.fromarray(np.uint8(img)).convert('RGB')
    #img_pilimg.save("mask.jpg")
    image_str = imageToStr(img_pilimg)
    #StrToImage(image_str)
    

    defect = { 'result' : image_str}
    defectList.append(defect)
    #print(defectList)
    jsons = json.dumps(defectList)
    
    return jsons