LEADERG AI Zoo official website: https://www.leaderg.com/ai-zoo

Documents are inside of solution folders.


How to use:

1. If not installed yet, please install latest Nvidia GPU driver from https://www.nvidia.com/Download/index.aspx

2. If not installed yet, please install third_party_tools\windows\vc_redist_x64-vc2010.exe

3. If not installed yet, please install third_party_tools\windows\vc_redist_x64-vc2015-vc2022.exe

4. Please set OS default browser to be Chrome or Firefox.

5. If not activated yet, please execute activate.bat to activate LEADERG AI Zoo.

6. Please execute AI-ZOO.exe to start user interface.


We make AI simple.

If you want to buy "AI software, computer, medical solutions", please contact us:
Email: leaderg@leaderg.com

